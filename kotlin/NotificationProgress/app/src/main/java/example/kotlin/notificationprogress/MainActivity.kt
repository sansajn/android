package example.kotlin.notificationprogress

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val channelId = "sample_channel_id4"
		val notificationId = 1
		createNotificationChannel(channelId)

		button_view.setOnClickListener {
			val notif = NotificationCompat.Builder(this, channelId)
				.setContentTitle("Download Task")
				.setContentText("Downloading your file ...")
				.setSmallIcon(R.mipmap.ic_launcher_round)

			val max = 25
			var progress = 0
			val handler = Handler()

			with (NotificationManagerCompat.from(this)) {
				notif.setProgress(max, progress, false)
				notify(notificationId, notif.build())

				Thread(Runnable {
					Log.d("tag", "thread running ...")
					while (progress < max) {
						progress += 1
						try {
							Thread.sleep(1000)
						} catch (e: InterruptedException) {
							e.printStackTrace()
						}

						handler.post(Runnable {
							if (progress == max) {
								notif.setContentText("Download complete.")
								notif.setProgress(0, 0, false)
							} else {
								val percentage = (progress * 100) / max
								notif.setContentText("$percentage% complete $progress of $max")
								notif.setProgress(max, progress, false)
							}

							notify(notificationId, notif.build())
						})
					}
				}).start()
			}

		}

	}

	private fun createNotificationChannel(channelId: String) {
		val name = "sample_channel"
		val channelDescription = "Channel Description"
		val importance = NotificationManager.IMPORTANCE_LOW

		val channel = NotificationChannel(channelId, name, importance)
		channel.apply {
			description = channelDescription
		}

		val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
		notificationManager.createNotificationChannel(channel)
	}

}
